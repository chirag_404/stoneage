﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(StoneAge.Web.Startup))]
namespace StoneAge.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
