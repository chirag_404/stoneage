﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoneAge.DTO
{
	public class Product:BaseDTO
	{
		//[Key]
		public Decimal Price { get; set; }
		public Category category { get; set; }
	}
}
