﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using StoneAge.DTO;
using System.ComponentModel.DataAnnotations;

namespace StoneAge.Database
{
	public class SAContext: DbContext
	{
		public SAContext():base("StoneAgeConnection")
		{

		}
		//[Key]
		public DbSet<Product> products { get; set; }
		public DbSet<Category> categories { get; set; }
	}
}
